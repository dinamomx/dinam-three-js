import './index.css';
import * as THREE from 'three';
//Controles orbitales
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
//Carga de modelos
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';


// Configuracion basica
//Se necesitan 3 cosas basicas para trabajar en three js 
//1.una escenea == contenedor
//2.Una camara la mas comun es de pespectiva
//3. Render inserta nuestro contenido en la escena o hace que se vea


const scene = new THREE.Scene();

//Argumentos de esta funcion
//1.field of view 
//2.Aspect radio
//3. view frustrum
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

const renderer = new THREE.WebGLRenderer({
  canvas: document.querySelector('#bg'),
});

renderer.setPixelRatio(window.devicePixelRatio);//definimos el radio de pixeles
renderer.setSize(window.innerWidth, window.innerHeight);//Definimos el tamaño
camera.position.setZ(30);
camera.position.setX(-3);


// Elementos a la escena (Modelos)

//MODELOS EXTERNOS
const loader = new GLTFLoader();//Creamos instancia para cargar modelos

let mixer;

//recibe 2 parametros (ruta, funcion de renderizado)
loader.load( './Public/models/walktest.glb', function ( gltf ) {
	scene.add( gltf.scene );

  //Agregando animacion
  mixer = new THREE.AnimationMixer(gltf.scene);//creamos nueva instancia de mixer
  const clips = gltf.animations;//creamos los clips
  const clip = THREE.AnimationClip.findByName(clips, 'Walk_A');//buscamos por nombre de clip
  const action = mixer.clipAction(clip);//cargamos la accion
  action.play();//la reproducimos


}, undefined, function ( error ) {
	console.error( error );
} );


// Lights

//Luz puntual de color blanca
const pointLight = new THREE.PointLight(0xffffff);
pointLight.position.set(5, 5, 5);

// //Luz ambiental de color blanco
const ambientLight = new THREE.AmbientLight(0xffffff);
scene.add(pointLight, ambientLight);


// Helpers para luz y para luz
const lightHelper = new THREE.PointLightHelper(pointLight)
const gridHelper = new THREE.GridHelper(200, 50);
scene.add(lightHelper, gridHelper)

//Control orbital 
//Nos ayuda a movernos con el mouse por el canva
const controls = new OrbitControls(camera, renderer.domElement);

const reloj = new THREE.Clock();
function animate(){ 
  
  //Validamos si el mixer ya existe
  if(mixer)
    mixer.update(reloj.getDelta());
  requestAnimationFrame(animate);
  //orbit controls
  //Para actualizar la posicion de nuestro control orbital
  controls.update();

  renderer.render(scene, camera);

}

animate();